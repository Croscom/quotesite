class QuotesController < ApplicationController
  def index
    @quotes = Quote.all.order('created_at DESC')
  end

  def new
    @quote = Quote.new
  end

  def create
    @quote = Quote.new(post_params)
    if @quote.save
      redirect_to action: 'index'
    else
      render 'new'
    end
  end

  def show
    @quote = Quote.find(params[:id])
  end

private
  
  def post_params
    params.require(:quote).permit(:content, :author)
  end
end
